#!/bin/bash
source ~/ros_ws/devel/setup.bash

export ROSCONSOLE_FORMAT='[${severity}] [${node}]: ${message}'

export ROBOT=mbot01

export ROBOT_ENV=ist-lab

export ROS_MASTER_URI=http://10.1.15.14:11311

%export ROS_IP=10.1.15.15

exec "$@"
