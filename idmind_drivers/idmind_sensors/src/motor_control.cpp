#include "idmind_sensors/motor_control.h"
using namespace std;
int button_active;
bool charging;
bool motors_enable;
ros::Time last_cmd_detect;
MotorControl::MotorControl() : n_("~")
{
  bat_info_sub = n_.subscribe<idmind_sensors::Electronics>("/idmind_motors/Battery", 1, &MotorControl::batCb, this);
  cmd_vel_sub = n_.subscribe<geometry_msgs::Twist>("/cmd_vel", 1, &MotorControl::velCb, this);
  activateMotorsSrv = n_.serviceClient<idmind_sensors::DockUndock>("/mbot_driver/sensors/activate_motors");
  dockSrv = n_.serviceClient<idmind_sensors::DockUndock>("/mbot_driver/sensors/dock_undock");

  motors_enable = false;
}

void MotorControl::runPeriodically()
{
  frequency_ = 20.0;

  ros::Rate r(frequency_);
  while(n_.ok())
  {
    ros::spinOnce();

    r.sleep();
  }
}

void MotorControl::velCb(const geometry_msgs::Twist::ConstPtr& msg)
{
  last_cmd_detect = ros::Time::now();
  cout<<"got speed"<<endl;
  
  cout<<"motor status "<<motors_enable<<endl;
  if(!motors_enable)
  {
    idmind_sensors::DockUndock msg;
    msg.request.control = 1;
    activateMotorsSrv.call(msg);
    motors_enable = true;
    std::cout<<"engage"<<endl;
  }
}

void MotorControl::batCb(const idmind_sensors::Electronics::ConstPtr& msg)
{  
  if(msg->charger_voltage > 17)
    charging = true;
  else
    charging = false;
    
  if(ros::Time::now() - last_cmd_detect > ros::Duration(5))
  {
    if(!charging && motors_enable)
    {
      idmind_sensors::DockUndock msg;
      msg.request.control = 0;
      activateMotorsSrv.call(msg);
      motors_enable = false;
      std::cout<<"Disengage"<<std::endl;
    }
  }
    
  if(charging && !motors_enable)
  {
    idmind_sensors::DockUndock msg;
    msg.request.control = 4;
    dockSrv.call(msg);
    motors_enable = true;
    std::cout<<"engage to dock"<<endl;
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "motor_control");

  MotorControl motor_control;

  motor_control.runPeriodically();

  return 0;
}
