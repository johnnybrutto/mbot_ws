/*
 * Copyright [2016] <Instituto Superior Tecnico>
 *
 * Author: Oscar Lima (olima@isr.tecnico.ulisboa.pt)
 * 
 * Listens to odom topic and corrects the twist inside it (must be expresed w.r.t base_link)
 * If your odom data is correct then you dont need this node
 * 
 */

#ifndef MBOT_BRINGUP_ODOM_FIXER_H
#define MBOT_BRINGUP_ODOM_FIXER_H

#include <ros/ros.h>
#include <ros/package.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <vector>

class OdomFixer
{
    public:
        OdomFixer();
        ~OdomFixer();

        // callback for event_in received msg
        void faultyOdomCallBack(const nav_msgs::Odometry::ConstPtr& msg);

        // transform a Vector stamped to a different reference frame
        bool transformVector(geometry_msgs::Vector3Stamped &vector, std::string &target_frame, 
             geometry_msgs::Vector3Stamped &transformed_vector, tf2_ros::Buffer &buffer);
        
        // ros node main loop
        void update(tf2_ros::Buffer &buffer);

    private:
        // ros related variables
        ros::NodeHandle nh_;
        ros::Publisher pub_fixed_odom_;
        ros::Subscriber sub_faulty_odom_;

        // flag used to know when we have received a callback
        bool is_odom_faulty_msg_received_;

        // stores the received msg in event_in callback (runScriptCallBack)
        nav_msgs::Odometry faulty_odom_msg_;

        // stores the callback msg received in faulty odom topic
        nav_msgs::Odometry fixed_odom_msg_;
        
        // to store the tf tolerance
        double transform_tolerance_;

};
#endif  // MBOT_BRINGUP_ODOM_FIXER_H
