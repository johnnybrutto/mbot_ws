/* 
 * Copyright [2016] <Instituto Superior Tecnico>  
 * 
 * Author: Oscar Lima (olima@isr.tecnico.ulisboa.pt)
 * 
 * Listens to odom topic and corrects the twist inside it (must be expresed w.r.t base_link)
 * If your odom data is correct then you dont need this node
 * 
 */

#include <mbot_bringup/odom_fixer.h>

OdomFixer::OdomFixer() : nh_("~"), is_odom_faulty_msg_received_(false)
{
    // subscriptions
    sub_faulty_odom_ = nh_.subscribe("/faulty_odom", 1, &OdomFixer::faultyOdomCallBack, this);

    // publications
    pub_fixed_odom_ = nh_.advertise<nav_msgs::Odometry>("/odom", 1);

    // parameters
    nh_.param("transform_tolerance", transform_tolerance_, 0.5);
    
}

OdomFixer::~OdomFixer()
{
    // shut down publishers and subscribers
    sub_faulty_odom_.shutdown();
    pub_fixed_odom_.shutdown();
}

void OdomFixer::faultyOdomCallBack(const nav_msgs::Odometry::ConstPtr& msg)
{
    faulty_odom_msg_ = *msg;
    is_odom_faulty_msg_received_ = true;
}

bool OdomFixer::transformVector(geometry_msgs::Vector3Stamped &vector, std::string &target_frame, 
             geometry_msgs::Vector3Stamped &transformed_vector, tf2_ros::Buffer &buffer)
{   
    // check if the vector stamped has frame_id
    if(vector.header.frame_id == "")
    {
        ROS_ERROR("Attempting to get origin of a frame with empty frame_id !");
        return false;
    }
    
    // transform vector
    buffer.transform(vector, transformed_vector, target_frame, 
                            ros::Duration(transform_tolerance_));
    
    // filling the new vector frame_id
    transformed_vector.header.frame_id = target_frame;
    
    return true;
}

void OdomFixer::update(tf2_ros::Buffer &buffer)
{   
    // listen to callbacks
    ros::spinOnce();

    if (!is_odom_faulty_msg_received_) return;

    // reset flag
    is_odom_faulty_msg_received_ = false;
    
    fixed_odom_msg_.header.frame_id = "odom";
    fixed_odom_msg_.header.stamp = ros::Time::now();
    fixed_odom_msg_.pose = faulty_odom_msg_.pose;
    fixed_odom_msg_.twist.twist.angular = faulty_odom_msg_.twist.twist.angular;
    
    geometry_msgs::Vector3Stamped vector_msg, transformed_vector;

    vector_msg.header.frame_id = "odom";
    vector_msg.vector.x = faulty_odom_msg_.twist.twist.linear.x;
    vector_msg.vector.y = faulty_odom_msg_.twist.twist.linear.y;
    vector_msg.vector.z = 0.0;
    
    std::string target_frame = "base_link";
    
    transformVector(vector_msg, target_frame, transformed_vector, buffer);
    
    fixed_odom_msg_.twist.twist.linear.x = transformed_vector.vector.x;
    fixed_odom_msg_.twist.twist.linear.y = transformed_vector.vector.y;
    fixed_odom_msg_.twist.twist.linear.z = 0.0;
    fixed_odom_msg_.child_frame_id = faulty_odom_msg_.child_frame_id;
    
    // publish the corrected odom value
    pub_fixed_odom_.publish(fixed_odom_msg_);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "odom_fixer_node");

    ROS_INFO("Node is going to initialize ...");

    // create object of the node class (OdomFixer)
    OdomFixer odom_fixer_node;

    // setup node frequency
    double node_frequency = 0.0;
    ros::NodeHandle nh("~");
    nh.param("node_frequency", node_frequency, 20.0);  // TODO: do rostopic hz on /odom topic and see the value, adjust this one accordingly
    ROS_INFO("Node will run at : %lf [hz]", node_frequency);
    ros::Rate loop_rate(node_frequency);

    ROS_INFO("Node initialized.");

    // transform listener setup (tf2)
    tf2_ros::Buffer buffer;
    tf2_ros::TransformListener tf_listener(buffer);
    
    while (ros::ok())
    {
        // main loop function
        odom_fixer_node.update(buffer);

        // sleep to control the node frequency
        loop_rate.sleep();
    }

    return 0;
}
