#ifndef IDMIND_MOTOR_CONTROL_H
#define IDMIND_MOTOR_CONTROL_H

#include <ros/ros.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/Twist.h>
#include "idmind_sensors/Electronics.h"
#include "idmind_sensors/DockUndock.h"
class MotorControl
{
public:
  MotorControl();

  void runPeriodically();
  void batCb(const idmind_sensors::Electronics::ConstPtr& msg);
  void velCb(const geometry_msgs::Twist::ConstPtr& msg);
  
private:
  ros::NodeHandle n_;
  double frequency_;
  ros::Subscriber bat_info_sub, cmd_vel_sub;
  ros::ServiceClient dockSrv, activateMotorsSrv;


  
};

#endif
