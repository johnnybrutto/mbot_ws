idmind_drivers.zip
==================

Original files sent from idmind to ISR in response to our query
to have open source drivers for the mbot robot.

Some modifications have been made to this drivers to adapt the
code to our platform. The zip file contained on this folder does
not work out of the box with the mbot, however is here as a backup
of the original version sent by idmind.
