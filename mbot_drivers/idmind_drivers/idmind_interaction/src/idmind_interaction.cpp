#include "idmind_interaction/idmind_interaction.h"

IdmindInteraction::IdmindInteraction()
    : n_("~"), serial_("/dev/mbot/interacboard", 115200, 5),
      capacitive_count_(5,0), lights_(0, std::vector<uint8_t>(0)), last_head_vel_(0),
      green_light_(false), send_mouth_(false), send_lights_(false), send_head_(false),
      is_needed_head_capacitive_sensor_(false), is_needed_head_recovery_(false)
{
  mouth_sub_ = n_.subscribe<std_msgs::UInt8MultiArray>("mouth", 100, &IdmindInteraction::mouthCallback, this);
  lights_sub_ = n_.subscribe<std_msgs::UInt8MultiArray>("lights", 100, &IdmindInteraction::lightsCallback, this);
  head_sub_ = n_.subscribe<std_msgs::UInt8MultiArray>("head", 100, &IdmindInteraction::headCallback, this);

  // we publish as sensor_msgs/JointState, not as UInt8MultiArray
  //head_angle_mode_pub_ = n_.advertise<std_msgs::UInt8MultiArray>("head_angle_mode", 100);
  head_angle_mode_pub_ = n_.advertise<sensor_msgs::JointState>("mbot_head_angle_joint_state", 1);
  capacitive_pub_ = n_.advertise<std_msgs::UInt16MultiArray>("capacitive", 100);

  projector_serv_ = n_.advertiseService("projector", &IdmindInteraction::projectorService, this);

  capacitive_calibration_ = n_.createTimer(ros::Duration(600), &IdmindInteraction::calibrateCapacitiveTimer, this);

  n_.param<int>("capacitive_true_positives", capacitive_true_positives_, 5);
  n_.param<bool>("is_needed_head_recovery", is_needed_head_recovery_, false);
  if(is_needed_head_recovery_)
      n_.param<int>("head_recovery_time", head_recovery_time_, 10);
  n_.param<bool>("is_needed_head_capacitive_sensor", is_needed_head_capacitive_sensor_, false);
  n_.param<bool>("initialize", initialize_, false);

  mouth_command_ = 0x40;
  lights_command_ = 0x43;
  head_command_ = 0x44;
  projector_command_ = 0x47;
  head_angle_command_ = 0x50;
  capacitive_command_ = 0x52;
  calibrate_capacitive_ = 0x48;
  // head_mode_command_ = 0x56;
  head_recovery_time_command_ = 0x49; // isr mbot05 does not has this command

  mouth_[0] = mouth_command_;
  light_[0] = lights_command_;

  if (serial_.ready_)
  {
    ROS_INFO("\e[32m%s ---> SUCCESSFUL <---\e[0m", ros::this_node::getName().c_str());
    green_light_ = true;
  }
  else
    ROS_ERROR("%s ---> FAILED <---", ros::this_node::getName().c_str());
}

void IdmindInteraction::drawMouthShape(int shape)
{
	for (int i = 0; i < 32; i++)
	{
		mouth_[i+1] = (char)0;
	}
	if(shape == 0)
	{
		for (int w = 0; w < 32; w++)
		{
			for (int h = 0; h < 8; h++)
			{
				//if(h == 7 )
				if ((h == 1 && (w == 3 || w == 32-3))||
				    (h == 2 && ((w > 2 && w < 6) || (w > 32-6 && w < 32 - 2)))||
				    (h == 3 && (w > 4  &&  w < 32 - 4))||
				    (h == 4 && (w > 5 &&  w < 32 - 5))||
				    (h == 5 && (w > 6 &&  w < 32 - 6)))
				{
					mouth_[32-w] += pow(2, 7-h);
				}
				else
					mouth_[32-w] += 0;
			}
		}
	}
	else if(shape == 1)
	{
		for (int w = 0; w < 32; w++)
		{
			for (int h = 0; h < 8; h++)
			{
				//if(h == 7 )
				if ((h == 5 && (w == 5 || w == 32-5))||
				    (h == 4 && ((w > 4 && w < 8) || (w > 32-8 && w < 32 - 4)))||
				    (h == 3 && (w > 6 &&  w < 32 - 6))||
				    (h == 2 && (w > 7 &&  w < 32 - 7))||
				    (h == 1 && (w > 9 &&  w < 32 - 9)))
				{
					mouth_[32-w] += pow(2, 7-h);
				}
				else
					mouth_[32-w] += 0;
			}
		}
	}
	else if(shape == 2)
	{
		for (int w = 0; w < 32; w++)
		{
			for (int h = 0; h < 8; h++)
			{
				//if(h == 7 )
				if ((h == 3 && (w > 6 &&  w < 32 - 6))||
				    (h == 2 && (w > 7 &&  w < 32 - 7))||
				    (h == 1 && (w > 9 &&  w < 32 - 9)))
				{
					mouth_[32-w] += pow(2, 7-h);
				}
				else
					mouth_[32-w] += 0;
			}
		}
	}
	else if(shape == 3)
	{
		for (int w = 0; w < 32; w++)
		{
			for (int h = 0; h < 8; h++)
			{
				mouth_[32-w] += 0;
			}
		}
	}
	sendMouthCommand();
}

void IdmindInteraction::initialize()
{
  if (initialize_)
  {
    head_[0] = 70;
    head_[1] = 30;
    sendHeadCommand();
    ros::Duration(1.0).sleep();
    head_[0] = 110;
    sendHeadCommand();
    ros::Duration(2.0).sleep();
    head_[0] = 90;
    sendHeadCommand();
    ros::Duration(1.0).sleep();

    std::fill(mouth_+1, mouth_+sizeof(mouth_) ,255);
    sendMouthCommand();
    ros::Duration(1.0).sleep();
    std::fill(mouth_+1, mouth_+sizeof(mouth_) ,0);
    sendMouthCommand();
    ros::Duration(1.0).sleep();


    for (int i=0; i<6; i++)
    {
      std::vector<uint8_t> row(5, 0);
      row[0] = i;
      row[4] = 100;

      for (int j=1; j<4; j++)
      {
        row[j] = 100;
        lights_.push_back(row);
        sendLightsCommand();
        ros::Duration(1.0).sleep();
        row[j] = 0;
        lights_.push_back(row);
        sendLightsCommand();
        ros::Duration(1.0).sleep();
      }
    }

    lights_.clear();
  }
}

void IdmindInteraction::runPeriodically()
{
  if(is_needed_head_capacitive_sensor_)
  {
      calibrateCapacitive();
      ros::Duration(0.005).sleep();
  }

  if(is_needed_head_recovery_)
  {
      setHeadRecoveryTime(head_recovery_time_*1e3);
      ros::Duration(0.005).sleep();
  }

  ros::Rate r(10.0);
  while(n_.ok())
  {
    ros::spinOnce();

    if (send_mouth_) sendMouthCommand();
    ros::Duration(0.005).sleep();

    if (send_head_) sendHeadCommand();
    ros::Duration(0.005).sleep();

    if (send_lights_)
    {
      while(lights_.size() > 0)
      {
        sendLightsCommand();
        ros::Duration(0.005).sleep();
      }

      lights_.clear();
    }

    readPublishHeadAngleMode();
    ros::Duration(0.005).sleep();

    if(is_needed_head_capacitive_sensor_)
        readPublishCapacitive();

    r.sleep();
  }
}

void IdmindInteraction::shutdown()
{
  ros::Duration(0.005).sleep();

  std::fill(mouth_+1, mouth_+sizeof(mouth_) , 0);
  sendMouthCommand();
  ros::Duration(0.005).sleep();

  lights_.clear();
  for (int i=0; i<6; i++)
  {
    std::vector<uint8_t> row(5, 0);
    row[0] = i;
    lights_.push_back(row);
    sendLightsCommand();
    ros::Duration(0.005).sleep();
  }
}

void IdmindInteraction::mouthCallback(const std_msgs::UInt8MultiArray::ConstPtr& msg)
{
  for (int i=0; i<32; i++)
    mouth_[i+1] = (*msg).data[i];

  send_mouth_ = true;
}

void IdmindInteraction::sendMouthCommand()
{
  if (serial_.write(mouth_, 33))
  {
    uint8_t buffer[4];
    if (!serial_.read(buffer, 4, true) || buffer[0] != mouth_command_)
      ROS_ERROR("%s --> Failed to send mouth command.", ros::this_node::getName().c_str());
  }

  send_mouth_ = false;
}

void IdmindInteraction::RGB_Leds_Control(char device, char Red, char Green, char Blue, char time)
{
	uint8_t command[6];
	command[0] = 0x43;
	command[1] = device;
	command[2] = Red;
	command[3] = Green;
	command[4] = Blue;
	command[5] = time;

	
	if (serial_.write(command, 6))
	{
		uint8_t buffer[4];
		if (!serial_.read(buffer, 4, true) || buffer[0] != lights_command_)
		ROS_ERROR("%s --> Failed to send lights command (device %d).", ros::this_node::getName().c_str(), light_[1]);
	}

  send_lights_ = false;
}


void IdmindInteraction::lightsCallback(const std_msgs::UInt8MultiArray::ConstPtr& msg)
{
  std::vector<uint8_t> row;
  lights_.push_back(row);

	RGB_Leds_Control((*msg).data[0],(*msg).data[1],(*msg).data[2],(*msg).data[3],(*msg).data[4]);
  //~ for (int i=0; i<5; i++)
    //~ lights_[lights_.size()-1].push_back((*msg).data[i]);

  //~ send_lights_ = true;
}

void IdmindInteraction::sendLightsCommand()
{
  for (int i=0; i<5; i++)
    light_[i+1] = lights_[lights_.size()-1][i];

  lights_.erase(lights_.end()-1);

  if (serial_.write(light_, 6))
  {
    uint8_t buffer[4];
    if (!serial_.read(buffer, 4, true) || buffer[0] != lights_command_)
      ROS_ERROR("%s --> Failed to send lights command (device %d).", ros::this_node::getName().c_str(), light_[1]);
  }

  send_lights_ = false;
}

void IdmindInteraction::headCallback(const std_msgs::UInt8MultiArray::ConstPtr& msg)
{
  head_[0] = (*msg).data[0];
  head_[1] = (*msg).data[1];

  ROS_DEBUG("cmd_head msg received : desired angle %d, velocity %d", (int)head_[0], (int)head_[1]);

  send_head_ = true;
}

void IdmindInteraction::sendHeadCommand()
{
  if (head_[1] != last_head_vel_)
  {
    uint8_t velocity[] = {head_command_+1, head_[1]};
    if (serial_.write(velocity, 2))
    {
      uint8_t buffer[4];
      if (!serial_.read(buffer, 4, true) || buffer[0] != head_command_+1)
        ROS_ERROR("%s --> Failed to send head velocity command.", ros::this_node::getName().c_str());
    }

    last_head_vel_ = head_[1];

    ros::Duration(0.005).sleep();
  }

  uint8_t position[] = {head_command_, head_[0]};
  if (serial_.write(position, 2))
  {
    uint8_t buffer[4];
    if (!serial_.read(buffer, 4, true) || buffer[0] != head_command_)
      ROS_ERROR("%s --> Failed to send head position command.", ros::this_node::getName().c_str());
  }

  send_head_ = false;
}

bool IdmindInteraction::projectorService(idmind_interaction::Projector::Request& req, idmind_interaction::Projector::Response& res)
{
  uint8_t command[] = {projector_command_, req.on};
  if(req.on==0){
            system("xrandr --output HDMI1 --auto --output HDMI2 --off");
            system("xrandr --output HDMI1 --rotate left");
            system("xdotool search \"Interface\" windowactivate");
  }
  if (serial_.write(command, 2))
  {
    uint8_t buffer[4];
    if (serial_.read(buffer, 4, true) && buffer[0] == projector_command_)
    {
        ros::Duration(3.0).sleep();
        if(req.on==1){
            system("xrandr --output HDMI1 --off --output HDMI2 --auto");
            ros::Duration(5.0).sleep();
            system("xdotool search \"Interface\" windowactivate");
        }
      res.success = true;
      return true;
    }
    else
      ROS_ERROR("%s --> Failed to send projector command (%d).", ros::this_node::getName().c_str(), req.on);
  }

  return false;
}

bool IdmindInteraction::setHeadRecoveryTime(int recovery_time)
{
  uint8_t command[] = {head_recovery_time_command_,
                       static_cast<uint8_t>((recovery_time >> 8) & 0xFF),
                       static_cast<uint8_t>(recovery_time & 0xFF)};

  if (serial_.write(command, 3))
  {
    uint8_t buffer[4];
    if (serial_.read(buffer, 4, true) && buffer[0] == head_recovery_time_command_)
    {
      ROS_INFO("%s --> Successfully set head recovery time.", ros::this_node::getName().c_str());
      return true;
    }
    else
      ROS_ERROR("%s --> Failed to set head recovery time.", ros::this_node::getName().c_str());
  }

  return false;
}

void IdmindInteraction::readPublishHeadAngleMode()
{
  //std_msgs::UInt8MultiArray head_angle_mode;
  //head_angle_mode.data.resize(2, 0);

  // to publish joint state of the mbot head
  sensor_msgs::JointState joint_state_msg;

  // fill head pan motor joint state msg content
  joint_state_msg.header.frame_id = std::string("head_static_link");
  joint_state_msg.header.stamp = ros::Time::now();
  joint_state_msg.name.push_back(std::string("head_link_static_to_head_link_joint"));
  // driver does not support velocity or effort for head joint
  joint_state_msg.velocity.push_back(0.0);
  joint_state_msg.effort.push_back(0.0);
  
  if (serial_.write((uint8_t)head_angle_command_))
  {
    uint8_t buffer[5];
    if (serial_.read(buffer, 5, true) && buffer[0] == head_angle_command_)
    {
        // head robot home position is 90 degree, lets make it 0, so that -90 degree is looking left,
        // +90 degree is looking right, 0 degree is looking front
        // also transform from degrees to radians
        joint_state_msg.position.push_back((((double)buffer[1] - 90.0) * 3.141592653589793) / 180.0);
    }
    else
      ROS_ERROR("%s --> Failed to read the head angle.", ros::this_node::getName().c_str());
  }

  /*ros::Duration(0.005).sleep();

  if (serial_.write((uint8_t)head_mode_command_))
  {
    uint8_t buffer[7];
    if (serial_.read(buffer, 7, true) && buffer[0] == head_mode_command_)
      head_angle_mode.data[1] = buffer[3];
    else
      ROS_ERROR("%s --> Failed to read the head mode.", ros::this_node::getName().c_str());
  }*/

  //head_angle_mode_pub_.publish(head_angle_mode);

  // instead of publishin manualy the tf between base link and head link we publish sensor_msgs/JointState and joint
  // state publisher takes care of publishing the transform
  /*static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin( tf::Vector3(0.05212, 0, 0.722) );
  tf::Quaternion q;
  float yaw = -(head_angle_mode.data[0] - 90) * M_PI / 180;
  q.setRPY(0, 0,yaw);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "head_link"));*/

  // Publish head neck pan motor joint state
  head_angle_mode_pub_.publish(joint_state_msg);

}

void IdmindInteraction::readPublishCapacitive()
{
  if (ros::Time::now() > capacitive_calibrated_ + ros::Duration(8.0))
  {
    if (serial_.write((uint8_t)capacitive_command_))
    {
      uint8_t buffer[6];
      if (serial_.read(buffer, 6, true) && buffer[0] == capacitive_command_)
      {
        std_msgs::UInt16MultiArray capacitive;
        capacitive.data.resize(5, 0);

        for (int i = 0; i < 5; i++)
        {
			capacitive.data[i] = ((buffer[1] & static_cast<uint8_t>(pow(2, i))) != 0) ? 1 : 0; // check boolean state from capacitive sensor
			std::cout<<"capacitive data "<<i<<": "<<capacitive.data[i]<<std::endl;
        }

        capacitive_pub_.publish(capacitive);
      }
      else
        ROS_ERROR("%s --> Failed to read capacitive sensors.", ros::this_node::getName().c_str());
    }
  }
}

void IdmindInteraction::calibrateCapacitive()
{
  if (serial_.write((uint8_t)calibrate_capacitive_))
  {
    uint8_t buffer[4];
    if (serial_.read(buffer, 4, true) && buffer[0] == calibrate_capacitive_)
    {
      ROS_INFO("%s --> Capacitive sensors calibrated.", ros::this_node::getName().c_str());

      capacitive_calibrated_ = ros::Time::now();
    }
    else
      ROS_ERROR("%s --> Failed to calibrate capacitive sensors.", ros::this_node::getName().c_str());
  }
}

void IdmindInteraction::calibrateCapacitiveTimer(const ros::TimerEvent&)
{
  calibrateCapacitive();
}

int main(int argc, char** argv){
  ros::init(argc, argv, "idmind_interaction");

  IdmindInteraction interaction;

  interaction.initialize();

  if (interaction.green_light_)
    interaction.runPeriodically();

  interaction.shutdown();

  return 0;
}
