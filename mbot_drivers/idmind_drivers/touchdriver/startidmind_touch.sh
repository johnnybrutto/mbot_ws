#!/bin/bash
ids=$(xinput --list |grep 'Touch Fusion'| awk -v search="$SEARCH" \
    '$0 ~ search {match($0, /id=[0-20]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
     )
xinput disable $ids
#xrandr --output HDMI1 --mode 1280x720
xrandr --output HDMI1 --mode 1280x720 --set audio force-dvi
xrandr -o left
xinput set-prop 'TPK USA LLC Touch Fusion 4.' 'Coordinate Transformation Matrix' 0 -1 1 1 0 0 0 0 1
python ~/ros_ws/src/mbot_drivers/idmind_drivers/touchdriver/touch_driver.py
